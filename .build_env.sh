#!/usr/bin/env sh
cp -r .config .ssh .vim ~/
ssh-keygen -t ed25519 -C "tatlock@gitlab.com" -f ~/.ssh/tatlock_gitlab
sudo apt-get install -y $(cat packages.txt)
pip3 install -r requirements.txt

# Vim config
for d in backup swap undo;do mkdir -p ~/.vim/$d;done
mkdir -p ~/.config/coc/extensions
vim -c 'CocInstall -sync coc-python|q'
