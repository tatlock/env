#!/usr/bin/env sh
git clone -j 6 --depth=1 --recurse-submodules --shallow-submodules https://gitlab.com/tatlock/env.git
cd env
./.build_env_test.sh
cd ..
rm -rf env
