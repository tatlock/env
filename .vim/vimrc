se t_Co=256 noek ut=300 sh=fish mouse=a
let g:gruvbox_italic=1
colo gruvbox
se bg=dark
hi Terminal ctermbg=0
filet plugin indent on
syntax on

" Wrap text
se lbr

" Split window below right
se sb spr

let g:netrw_liststyle=1
let g:netrw_keepdir=0
se wim=longest,list,full

" Indent guides
let g:indent_guides_enable_on_vim_startup=1

" Set directories
se bdir=~/.vim/backup// dir=~/.vim/swap// udir=~/.vim/undo//

" Highlight cursor line/column and show (relative) line numbers
se nu rnu cursorline cursorcolumn
aug numswitch
au!
au BufEnter,FocusGained,InsertLeave,WinEnter * se rnu
au BufLeave,FocusLost,InsertEnter,WinLeave * se nornu
aug END

" Indents/tabs
let g:ctab_disable_tab_maps=1
:se noet sts=0 sw=4 ts=4 cin cino=(0,u0,U0

" Swap colon/semicolon in normal and visual mode
nn ; :
nn : ;
vn ; :
no : ;

" Configure buffers
se hid

" Don't pass messages to |ins-completion-menu|.
se shm+=c

" Always show the signcolumn, otherwise it would shift the text each time diagnostics appear/become resolved.
se scl=yes

ino <silent><expr> <TAB> pumvisible() ? "\<C-n>" : <SID>check_back_space() ? "\<TAB>" : coc#refresh()
ino <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

fu! s:check_back_space() abort
  let col = col('.') - 1
  retu !col || getline('.')[col - 1]  =~# '\s'
endf

" Use <c-space> to trigger completion.
ino <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position
if exists('*complete_info')
  ino <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
el
  ino <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
en

" Use `[g` and `]g` to navigate diagnostics
nm <silent> [g <Plug>(coc-diagnostic-prev)
nm <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nm <silent> gd <Plug>(coc-definition)
nm <silent> gy <Plug>(coc-type-definition)
nm <silent> gi <Plug>(coc-implementation)
nm <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nn <silent> K :call <SID>show_documentation()<CR>

fu! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  el
    call CocAction('doHover')
  en
endf

" Formatting selected code.
xm <leader>f  <Plug>(coc-format-selected)
nm <leader>f  <Plug>(coc-format-selected)

" Add `:Format` command to format current buffer.
com! -nargs=0 Format :call CocAction('format')

" Status line setup
se nosmd
se ls=2
let g:lightline={'mode_map':{'n':'N','i':'I','R':'R','v':'V','V':'VL',"\<C-v>":'VB','c':'C','s':'S','S':'SL',"\<C-s>":'SB','t':'T'},
	\ 'active':{'left':[['mode','paste'],['gitbranch','readonly','filename','modified'],['coc_error','coc_warning','coc_hint','coc_info']],
	\           'right':[['column'],['bufnum'],['fileformat','fileencoding','filetype']]},
	\ 'component_expand':{'coc_error':'LightlineCocErrors','coc_warning':'LightlineCocWarnings','coc_info':'LightlineCocInfos','coc_hint':'LightlineCocHints','coc_fix':'LightlineCocFixes'},
	\ 'component_function':{'gitbranch':'FugitiveHead'}}
let g:lightline.component_type = {'coc_error':'error','coc_warning':'warning','coc_info':'tabsel','coc_hint':'middle','coc_fix':'middle'}
fu! s:lightline_coc_diagnostic(kind, sign) abort
  let info = get(b:, 'coc_diagnostic_info', 0)
  if empty(info) || get(info, a:kind, 0) == 0
    retu ''
  en
  retu printf('%d', info[a:kind])
endf
fu! LightlineCocErrors() abort
  retu s:lightline_coc_diagnostic('error', 'error')
endf
fu! LightlineCocWarnings() abort
  retu s:lightline_coc_diagnostic('warning', 'warning')
endf
fu! LightlineCocInfos() abort
  retu s:lightline_coc_diagnostic('information', 'info')
endf
fu! LightlineCocHints() abort
  retu s:lightline_coc_diagnostic('hints', 'hint')
endf
au User CocDiagnosticChange call lightline#update()

com Gitlog exec printf("Git log --graph --pretty=format:'%%<|(15,trunc)%%an  %%<|(%d,trunc)%%s  %%h %%ad' --date=format:'%%Y-%%m-%%d %%H:%%M' --all",winwidth(0)-max([len(line('$')), &nuw])-28)
