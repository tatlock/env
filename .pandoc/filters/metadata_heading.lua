function Pandoc(d)
	m = d.meta.date
	a = d.meta.author
	t = d.meta.title
	x = m ~= nil
	y = a ~= nil
	z = t ~= nil
	if x or y or z then
		d.blocks:insert(1, pandoc.HorizontalRule())
	end
	if x then
		d.blocks:insert(1, pandoc.Para(pandoc.utils.stringify(m)))
	end
	if y then
		d.blocks:insert(1, pandoc.Para(pandoc.utils.stringify(a)))
	end
	if z then
		d.blocks:insert(1, pandoc.Header(1, pandoc.utils.stringify(t)))
	end
	return d
end
