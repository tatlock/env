function Meta(m)
	a = m.author == nil
	d = m.date == nil
	if a or d then
		p = io.popen("git log -1 --pretty=format:'%an\n%cd' --date=format:'%A %d %B %Y, %H:%M' " .. table.concat(PANDOC_STATE.input_files, " ") .. " 2>&1")
		pa = p:read()
		pd = p:read()
		p:close()
		if pd ~= nil then
			if a then
				m.author = pa
			end
			if d then
				m.date = pd
			end
		end
	end
	return m
end
